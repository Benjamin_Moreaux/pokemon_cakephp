<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DresseurPokemonTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DresseurPokemonTable Test Case
 */
class DresseurPokemonTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DresseurPokemonTable
     */
    public $DresseurPokemon;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DresseurPokemon',
        'app.Dresseurs',
        'app.Pokemon'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DresseurPokemon') ? [] : ['className' => DresseurPokemonTable::class];
        $this->DresseurPokemon = TableRegistry::getTableLocator()->get('DresseurPokemon', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DresseurPokemon);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
