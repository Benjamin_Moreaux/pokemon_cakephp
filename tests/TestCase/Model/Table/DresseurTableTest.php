<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DresseurTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DresseurTable Test Case
 */
class DresseurTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DresseurTable
     */
    public $Dresseur;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Dresseur'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Dresseur') ? [] : ['className' => DresseurTable::class];
        $this->Dresseur = TableRegistry::getTableLocator()->get('Dresseur', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Dresseur);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
