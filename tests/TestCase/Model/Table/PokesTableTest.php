<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PokesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PokesTable Test Case
 */
class PokesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PokesTable
     */
    public $Pokes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Pokes',
        'app.Dresseur'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Pokes') ? [] : ['className' => PokesTable::class];
        $this->Pokes = TableRegistry::getTableLocator()->get('Pokes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Pokes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
