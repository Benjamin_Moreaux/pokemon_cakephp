<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseurPoke[]|\Cake\Collection\CollectionInterface $dresseurPokes
 */
?>

<head>
<style type="text/css">

.center{
    margin-left:240px;
    margin-top:2%;
    position: relative;
}

.centertab{
    width: auto;
    height: auto;
    margin-right: -40%;
}

.paginator{
    margin-left: 400px;
}

</style>
</head>


<div class="btn-group" role="group" aria-label="Basic example">
<div class="center">
  <button type="button" class="btn btn-dark"><?= $this->Html->link(__('New Dresseur Poke'), ['action' => 'add']) ?></button>
  <button type="button" class="btn btn-dark"><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></button>
  <button type="button" class="btn btn-dark"><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></button>
  <button type="button" class="btn btn-dark"><?= $this->Html->link(__('List Pokes'), ['controller' => 'Pokes', 'action' => 'index']) ?></button>
  <button type="button" class="btn btn-dark"><?= $this->Html->link(__('New Poke'), ['controller' => 'Pokes', 'action' => 'add']) ?></button>
</div>
</div>



<div class="fights index large-9 medium-8 columns content">
    <h3>DRESSEURS POKEMONS</h3>

<div class=centertab>

<table class="table table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Dresseur</th>
      <th scope="col">Pokemon</th>
      <th scope="col">Created</th>
      <th scope="col*">Modified</th>
      <th scope="col*">Favorie</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
            <?php foreach ($dresseurPokes as $dresseurPoke): ?>
            
  <tbody>
    <tr>
      <th scope="row"><?= $this->Number->format($dresseurPoke->id) ?></th>
      <td><?= $dresseurPoke->has('dresseur') ? $this->Html->link($dresseurPoke->dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $dresseurPoke->dresseur->id]) : '' ?></td>
      <td><?= $dresseurPoke->has('poke') ? $this->Html->link($dresseurPoke->poke->name, ['controller' => 'Pokes', 'action' => 'view', $dresseurPoke->poke->id]) : '' ?></td>
      <td><?= h($dresseurPoke->created) ?></td>
      <td><?= h($dresseurPoke->modified) ?></td>
      <td><?= h($dresseurPoke->is_fav) ?></td>      
      <td><?= $this->Html->link(__('View'), ['action' => 'view', $dresseurPoke->id]) ?>
          <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dresseurPoke->id]) ?>
          <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dresseurPoke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPoke->id)]) ?></td>

            <?php endforeach; ?>

    </tr>
  </tbody>
</table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
