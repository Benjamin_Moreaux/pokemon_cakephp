<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur[]|\Cake\Collection\CollectionInterface $dresseurs
 */
?>


<head>
<style type="text/css">
    .center{
        margin-left:350px;
        margin-top:2%;
        position: relative;
    }

    .centertab{
        width: auto;
        height: auto;
        margin-right: -40%;
    }



</style>
</head>


<div class="btn-group" role="group" aria-label="Basic example">
<div class="center">
  <button type="button" class="btn btn-dark"><?= $this->Html->link(__('New Dresseur'), ['action' => 'add']) ?></button>
  <button type="button" class="btn btn-dark"><?= $this->Html->link(__('List Dresseur Pokes'), ['controller' => 'DresseurPokes', 'action' => 'index']) ?></button>
  <button type="button" class="btn btn-dark"><?= $this->Html->link(__('New Dresseur Poke'), ['controller' => 'DresseurPokes', 'action' => 'add']) ?></button>
</div>
</div>



<div class="fights index large-9 medium-8 columns content">
    <h3>DRESSEURS</h3>

<div class=centertab>

<table class="table table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Prénom</th>
      <th scope="col">Nom</th>
      <th scope="col">Created</th>
      <th scope="col*">Modified</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <?php foreach ($dresseurs as $dresseur): ?>
            
  <tbody>
    <tr>
      <th scope="row"><?= $this->Number->format($dresseur->id) ?></th>
      <td><?= h($dresseur->first_name) ?></td>
      <td><?= h($dresseur->last_name) ?></td>
      <td><?= h($dresseur->created) ?></td>
      <td><?= h($dresseur->modified) ?></td>   
      <td><?= $this->Html->link(__('View'), ['action' => 'view', $dresseur->id]) ?>
          <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dresseur->id]) ?>
          <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dresseur->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseur->id)]) ?></td>

            <?php endforeach; ?>

    </tr>
  </tbody>
</table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
