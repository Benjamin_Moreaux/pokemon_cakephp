<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight[]|\Cake\Collection\CollectionInterface $fights
 */
?>

<head>
<style type="text/css">

.center{
    margin-left:380px;
    margin-top:2%;
    position: relative;
}

.centertab{
    width: auto;
    height: auto;
    margin-right: -40%;
}

.paginator{
    margin-left: 400px;
}

</style>
</head>

<div class="btn-group" role="group" aria-label="Basic example">
<div class="center">
  <button type="button" class="btn btn-dark"><?= $this->Html->link(__('New Fight'), ['action' => 'add']) ?></button>
  <button type="button" class="btn btn-dark"><?= $this->Html->link(__('List First Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></button>
  <button type="button" class="btn btn-dark"><?= $this->Html->link(__('New First Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></button>
</div>
</div>



<div class="fights index large-9 medium-8 columns content">
    <h3>COMBAT</h3>

<div class=centertab>

<table class="table table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Dresseur 1</th>
      <th scope="col">Dresseur 2</th>
      <th scope="col">Gagnant</th>
      <th scope="col*">Created</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
        <?php foreach ($fights as $fight): ?>
            
  <tbody>
    <tr>
      <th scope="row"><?= $this->Number->format($fight->id) ?></th>
      <td><?= $fight->has('first_dresseur') ? $this->Html->link($fight->first_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->first_dresseur->id]) : '' ?></td>
      <td><?= $fight->has('second_dresseur') ? $this->Html->link($fight->second_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->second_dresseur->id]) : '' ?></td>
      <td><?= $fight->has('winner_dresseur') ? $this->Html->link($fight->winner_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->winner_dresseur->id]) : '' ?></td>
      <td><?= h($fight->created) ?></td>      
      <td><?= $this->Html->link(__('View'), ['action' => 'view', $fight->id]) ?>
          <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $fight->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fight->id)]) ?></td>

            <?php endforeach; ?>

    </tr>
  </tbody>
</table>

</div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
